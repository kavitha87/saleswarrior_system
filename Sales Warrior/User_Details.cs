//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sales_Warrior
{
    using System;
    using System.Collections.Generic;
    
    public partial class User_Details
    {
        public int ID { get; set; }
        public string Password { get; set; }
        public string Login_Type { get; set; }
        public string UserName { get; set; }
    
        public virtual Sales_Rep Sales_Rep { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual Vendor Vendor { get; set; }
    }
}
