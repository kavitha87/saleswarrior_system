﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Sales_Warrior;
using Sales_Warrior.Models;
namespace Sales_Warrior.Controllers
{
    public class ListOfOrderController : Controller
    {
        Sales_WarriorEntities ord = new Sales_WarriorEntities();
        [HttpGet]
        public ActionResult ListOfOrder()
        {
            var Vendor_Name = (from p in ord.Orders
                               join vn in ord.Vendors
                               on p.Vendor_Id equals vn.Vendor_Id
                               select new
                               {
                                   vn.Vendor_Name,
                                   vn.Vendor_Id
                               }).ToList();
            var vlist = new SelectList(Vendor_Name.ToList(), "Vendor_Id", "Vendor_Name");
            ViewBag.Vendor_Name = vlist;
            return View();
        }
        //[HttpPost]
        //public ActionResult search()
        //{
        //    return View();
        //}

        [HttpGet]
        public ActionResult FillCity(string Vendor_Id)
        {
            var Suppliers = (from s in ord.Suppliers
                             join o in ord.Orders
                             on s.Sup_Id equals o.Sup_Id
                             where o.Vendor_Id == Vendor_Id
                             select new
                             {
                                 s.Sup_Id,
                                 s.Sup_Name
                             }).ToList().Distinct();
            ViewBag.Supplier = Suppliers.ToList();
            return Json(Suppliers.AsEnumerable(), JsonRequestBehavior.AllowGet);
            //    return this.Json(Suppliers, JsonRequestBehavior.AllowGet);
            //    var list = new SelectList(Suppliers.ToList(), "Sup_Id", "Sup_Name");
            //ViewBag.Supplier = list;
            ////var cities = db.Cities.Where(c => c.StateId == state);
            ////return View();
            //return Json(ViewBag.Supplier, JsonRequestBehavior.AllowGet);

        }

    }
}
    


