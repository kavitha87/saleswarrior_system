﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sales_Warrior;
namespace Sales_Warrior.Controllers
{
    public class LoginController : Controller
    {
        Sales_WarriorEntities obj = new Sales_WarriorEntities();
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Validate(Supplier le, Vendor ve, FFAdmin Fa, Sales_Rep Sr, string alt)
        {
            TempData["Name"] = Sr.UserName;

            var SalesLoginvalidation = (from lo in obj.Sales_Rep
                                        where lo.UserName == Sr.UserName && lo.Password == Sr.Password
                                        select new
                                        {lo.Sales_Rep_Id,lo.UserName}).FirstOrDefault(); 


            if (SalesLoginvalidation != null)
            {
                
                return RedirectToAction("Sales_Rep", "Sales_Rep"); 

            }
            
            else 
            {
                TempData["Name"] = le.UserName;
                var SupplierLoginvalidation = (from lo in obj.Suppliers
                                               where lo.UserName == le.UserName && lo.Password == le.Password
                                               select new {lo.Sup_Id,lo.UserName}).FirstOrDefault();
                if (SupplierLoginvalidation != null)
                {

                    return RedirectToAction("Supplier", "Supplier");
                }
                else
                {
                    TempData["Name"] = ve.UserName;
                    var VendorsLoginvalidation = (from lo in obj.Vendors
                                                   where lo.UserName == ve.UserName && lo.Password == ve.Password
                                                   select new { lo.Vendor_Id,lo.UserName}).FirstOrDefault();

                    if (VendorsLoginvalidation != null)
                    {
                       
                        return RedirectToAction("Vendor", "Vendor");
                    }
                    else
                    {
                        TempData["Name"] = Fa.UserName;
                        var FFAdminsLoginvalidation = (from lo in obj.FFAdmins
                                                      where lo.UserName == Fa.UserName && lo.Password == Fa.Password
                                                      select new { lo.Admin_Id,lo.UserName}).FirstOrDefault();
                        if (FFAdminsLoginvalidation !=null)

                        {
                            return RedirectToAction("FFAdmin", "FFAdmin");
                        }
                        else
                        {
                            //Response.Write("Invalid Username or Password");
                            ViewBag.message = String.Format("Invalid Username or Password", alt);
                            return View("Login");
                        }
                        

                    }
                    
                }

            }

        }

    }
}
