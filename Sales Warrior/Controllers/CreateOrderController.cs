﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sales_Warrior;
using System.Data.Entity;


namespace Sales_Warrior.Controllers
{
    

    public class CreateOrderController : Controller
    {
        Sales_WarriorEntities SW = new Sales_WarriorEntities();
        // GET: CreateOrder
        public ActionResult CreateOrder()
        {
            //var product = SW.Products.ToList();
            //SelectList list = new SelectList(product, "ID", "Prod_Id");

            var orderproduct = SW.Order_Product.ToList();
            SelectList list = new SelectList(orderproduct, "ID", "Prod_Id");
            //var order = SW.Orders.ToList();

            ViewBag.orderproduct = list;
            return View(orderproduct);
        }
        [HttpPost]
        public ActionResult Save()
        {
            return View();
        }
        
        
    }
}