﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales_Warrior.Controllers
{
    public class ProductSearchController : Controller
    {
        Sales_WarriorEntities obj = new Sales_WarriorEntities();
        // GET: ProductSearch
        public ActionResult Productsearch()
        {
            return View(obj.Product_Search(""));
        }
        [HttpPost]
        public ActionResult Productsearch(string ProductName)
        {

            return View(obj.Product_Search(ProductName));
        }
    }
}