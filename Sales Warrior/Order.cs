//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sales_Warrior
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            this.Order_Product = new HashSet<Order_Product>();
        }
    
        public int Order_Id { get; set; }
        public System.DateTime Order_Date { get; set; }
        public string Vendor_Id { get; set; }
        public int Admin_Id { get; set; }
        public string Sales_Rep_Id { get; set; }
        public string Sup_Id { get; set; }
        public string Status { get; set; }
        public int ID { get; set; }
    
        public virtual FFAdmin FFAdmin { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order_Product> Order_Product { get; set; }
        public virtual Sales_Rep Sales_Rep { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual Vendor Vendor { get; set; }
    }
}
