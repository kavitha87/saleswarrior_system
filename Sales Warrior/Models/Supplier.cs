﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales_Warrior.Models
{
    public class Supplier
    {
        public string Sup_Id { get; set; }
        public string Sup_Name { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string Email_Id { get; set; }
        public string Phone_Number { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int ID { get; set; }
        public List<SelectListItem> Suppliers { get; set; }
    }
}