﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales_Warrior.Models
{
    public class Vendor
    {
        public string Vendor_Id { get; set; }
        public string Vendor_Name { get; set; }
        public string Address { get; set; }
        public int Tax_Registration { get; set; }
        public string Email_ID { get; set; }
        public string Phone_Number { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int ID { get; set; }
        public List<SelectListItem> vendor { get; set; }
    }
}