﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sales_Warrior;
namespace Sales_Warrior.Models
{
    public class SOV
    {
        public IEnumerable<Supplier> Supplier { get; set; }
        public IEnumerable<Order> Order { get; set; }
        public IEnumerable<Vendor> Vendor { get; set; }

    }
}