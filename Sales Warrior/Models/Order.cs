﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales_Warrior.Models
{
    public class Order
    {
        public int Order_Id { get; set; }
        public System.DateTime Order_Date { get; set; }
        public string Vendor_Id { get; set; }
        public int Admin_Id { get; set; }
        public string Sales_Rep_Id { get; set; }
        public string Sup_Id { get; set; }
        public string Status { get; set; }
        public int ID { get; set; }
        public List<SelectListItem> Orders { get; set; }
    }
}