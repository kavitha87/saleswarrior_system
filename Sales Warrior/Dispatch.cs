//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sales_Warrior
{
    using System;
    using System.Collections.Generic;
    
    public partial class Dispatch
    {
        public int Airway_Bill_No { get; set; }
        public int Quantity { get; set; }
        public System.DateTime Dispatch_Date { get; set; }
        public int ID { get; set; }
        public int Order_Product_Id { get; set; }
    
        public virtual Order_Product Order_Product { get; set; }
        public virtual Order_Product Order_Product1 { get; set; }
    }
}
